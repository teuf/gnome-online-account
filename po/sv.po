# Swedish translation for gnome-online-accounts.
# Copyright © 2011-2015 Free Software Foundation, Inc.
# This file is distributed under the same license as the gnome-online-accounts package.
#
# Daniel Nylander <po@danielnylander.se>, 2011, 2012.
# Mattias Eriksson <snaggen@gmail.com>, 2014.
# Anders Jonsson <anders.jonsson@norsjovallen.se>, 2015.
# Sebastian Rasmussen <sebras@gmail.com>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-online-accounts\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-online-"
"accounts&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2015-09-15 09:29+0000\n"
"PO-Revision-Date: 2015-09-15 14:21+0200\n"
"Last-Translator: Anders Jonsson <anders.jonsson@norsjovallen.se>\n"
"Language-Team: Swedish <tp-sv@listor.tp-sv.se>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.3\n"

#. TODO: more specific
#: ../src/daemon/goadaemon.c:830 ../src/daemon/goadaemon.c:1090
#, c-format
msgid "Failed to find a provider for: %s"
msgstr "Kunde inte hitta en modul för: %s"

#: ../src/daemon/goadaemon.c:1018
msgid "IsLocked property is set for account"
msgstr "Egenskapen IsLocked är satt för konto"

#. TODO: more specific
#: ../src/daemon/goadaemon.c:1078
msgid "ProviderType property is not set for account"
msgstr "Egenskapen ProviderType är inte satt för konto"

#. TODO: more specific
#: ../src/goabackend/goaewsclient.c:237 ../src/goabackend/goahttpclient.c:174
#, c-format
msgid "Code: %u — Unexpected response from server"
msgstr "Kod: %u — Oväntat svar från server"

#. TODO: more specific
#: ../src/goabackend/goaewsclient.c:253
#, c-format
msgid "Failed to parse autodiscover response XML"
msgstr "Misslyckades med att tolka XML-svar för autodetektering"

#. TODO: more specific
#. Translators: the parameter is an XML element name.
#: ../src/goabackend/goaewsclient.c:264 ../src/goabackend/goaewsclient.c:279
#: ../src/goabackend/goaewsclient.c:294
#, c-format
msgid "Failed to find ‘%s’ element"
msgstr "Misslyckades med att hitta elementet ‘%s’"

#. TODO: more specific
#: ../src/goabackend/goaewsclient.c:315
#, c-format
msgid "Failed to find ASUrl and OABUrl in autodiscover response"
msgstr "Misslyckades med att hitta ASUrl och OABUrl i autodetekteringssvar"

#: ../src/goabackend/goaexchangeprovider.c:73
msgid "Microsoft Exchange"
msgstr "Microsoft Exchange"

#. TODO: more specific
#: ../src/goabackend/goaexchangeprovider.c:311
#: ../src/goabackend/goaexchangeprovider.c:985
#: ../src/goabackend/goalastfmprovider.c:286
#: ../src/goabackend/goaowncloudprovider.c:383
#: ../src/goabackend/goaowncloudprovider.c:1128
#, c-format
msgid "Did not find password with identity ‘%s’ in credentials"
msgstr "Hittade inte lösenord med identiteten ‘%s’ bland inloggningsuppgifterna"

#. Translators: the first %s is the username
#. * (eg., debarshi.ray@gmail.com or rishi), and the
#. * (%s, %d) is the error domain and code.
#.
#: ../src/goabackend/goaexchangeprovider.c:339
#: ../src/goabackend/goalastfmprovider.c:302
#: ../src/goabackend/goaowncloudprovider.c:406
#, c-format
msgid "Invalid password with username ‘%s’ (%s, %d): "
msgstr "Ogiltigt lösenord med användarnamn ‘%s’ (%s, %d): "

#: ../src/goabackend/goaexchangeprovider.c:492
#: ../src/goabackend/goaimapsmtpprovider.c:700
msgid "_E-mail"
msgstr "_E-post"

#: ../src/goabackend/goaexchangeprovider.c:493
#: ../src/goabackend/goaimapsmtpprovider.c:720
#: ../src/goabackend/goaimapsmtpprovider.c:740
#: ../src/goabackend/goalastfmprovider.c:443
#: ../src/goabackend/goaowncloudprovider.c:634
msgid "_Password"
msgstr "_Lösenord"

#: ../src/goabackend/goaexchangeprovider.c:496
msgid "_Custom"
msgstr "An_passat"

#: ../src/goabackend/goaexchangeprovider.c:507
#: ../src/goabackend/goaimapsmtpprovider.c:719
#: ../src/goabackend/goaimapsmtpprovider.c:739
#: ../src/goabackend/goakerberosprovider.c:719
#: ../src/goabackend/goalastfmprovider.c:442
#: ../src/goabackend/goaowncloudprovider.c:633
msgid "User_name"
msgstr "Användar_namn"

#: ../src/goabackend/goaexchangeprovider.c:508
#: ../src/goabackend/goaowncloudprovider.c:632
msgid "_Server"
msgstr "_Server"

#. --
#: ../src/goabackend/goaexchangeprovider.c:518
#: ../src/goabackend/goaimapsmtpprovider.c:752
#: ../src/goabackend/goakerberosprovider.c:724
#: ../src/goabackend/goalastfmprovider.c:451
#: ../src/goabackend/goaowncloudprovider.c:643
#: ../src/goabackend/goatelepathyprovider.c:649
msgid "_Cancel"
msgstr "_Avbryt"

#: ../src/goabackend/goaexchangeprovider.c:519
#: ../src/goabackend/goakerberosprovider.c:725
#: ../src/goabackend/goalastfmprovider.c:452
#: ../src/goabackend/goaowncloudprovider.c:644
msgid "C_onnect"
msgstr "Ansl_ut"

#: ../src/goabackend/goaexchangeprovider.c:535
#: ../src/goabackend/goaimapsmtpprovider.c:769
#: ../src/goabackend/goakerberosprovider.c:741
#: ../src/goabackend/goalastfmprovider.c:468
#: ../src/goabackend/goaowncloudprovider.c:660
msgid "Connecting…"
msgstr "Ansluter…"

#: ../src/goabackend/goaexchangeprovider.c:640
#: ../src/goabackend/goaexchangeprovider.c:837
#: ../src/goabackend/goaimapsmtpprovider.c:931
#: ../src/goabackend/goaimapsmtpprovider.c:966
#: ../src/goabackend/goaimapsmtpprovider.c:1054
#: ../src/goabackend/goaimapsmtpprovider.c:1318
#: ../src/goabackend/goaimapsmtpprovider.c:1391
#: ../src/goabackend/goakerberosprovider.c:1142
#: ../src/goabackend/goalastfmprovider.c:659 ../src/goabackend/goalastfmprovider.c:825
#: ../src/goabackend/goamediaserverprovider.c:475
#: ../src/goabackend/goaoauth2provider.c:1034 ../src/goabackend/goaoauthprovider.c:863
#: ../src/goabackend/goaowncloudprovider.c:771
#: ../src/goabackend/goaowncloudprovider.c:986
#: ../src/goabackend/goatelepathyprovider.c:459
#: ../src/goabackend/goatelepathyprovider.c:508
#: ../src/goabackend/goatelepathyprovider.c:683
#, c-format
msgid "Dialog was dismissed"
msgstr "Dialogrutan stängdes"

#: ../src/goabackend/goaexchangeprovider.c:678
#: ../src/goabackend/goaexchangeprovider.c:865
#: ../src/goabackend/goaimapsmtpprovider.c:996
#: ../src/goabackend/goaimapsmtpprovider.c:1089
#: ../src/goabackend/goaimapsmtpprovider.c:1342
#: ../src/goabackend/goaimapsmtpprovider.c:1416
#: ../src/goabackend/goalastfmprovider.c:693 ../src/goabackend/goalastfmprovider.c:844
#: ../src/goabackend/goaowncloudprovider.c:813
#: ../src/goabackend/goaowncloudprovider.c:1007
#, c-format
msgid "Dialog was dismissed (%s, %d): "
msgstr "Dialogrutan stängdes (%s, %d): "

#: ../src/goabackend/goaexchangeprovider.c:691
#: ../src/goabackend/goaimapsmtpprovider.c:1009
#: ../src/goabackend/goaimapsmtpprovider.c:1102
#: ../src/goabackend/goaowncloudprovider.c:826
msgid "_Ignore"
msgstr "_Ignorera"

#: ../src/goabackend/goaexchangeprovider.c:696
#: ../src/goabackend/goaexchangeprovider.c:884
#: ../src/goabackend/goaimapsmtpprovider.c:1014
#: ../src/goabackend/goaimapsmtpprovider.c:1107
#: ../src/goabackend/goaimapsmtpprovider.c:1361
#: ../src/goabackend/goaimapsmtpprovider.c:1435
#: ../src/goabackend/goakerberosprovider.c:1252
#: ../src/goabackend/goalastfmprovider.c:705 ../src/goabackend/goalastfmprovider.c:861
#: ../src/goabackend/goaowncloudprovider.c:831
#: ../src/goabackend/goaowncloudprovider.c:1026
msgid "_Try Again"
msgstr "_Försök igen"

#: ../src/goabackend/goaexchangeprovider.c:701
#: ../src/goabackend/goaexchangeprovider.c:877
msgid "Error connecting to Microsoft Exchange server"
msgstr "Fel vid anslutning till Microsoft Exchange-server"

#: ../src/goabackend/goafacebookprovider.c:76
msgid "Facebook"
msgstr "Facebook"

#: ../src/goabackend/goafacebookprovider.c:215
#: ../src/goabackend/goaflickrprovider.c:183
#: ../src/goabackend/goafoursquareprovider.c:189
#: ../src/goabackend/goagoogleprovider.c:221
#: ../src/goabackend/goawindowsliveprovider.c:195
#, c-format
msgid "Expected status 200 when requesting your identity, instead got status %d (%s)"
msgstr ""
"Förväntade status 200 vid begäran av din identitet, fick istället status %d (%s)"

#: ../src/goabackend/goafacebookprovider.c:234
#: ../src/goabackend/goafacebookprovider.c:246
#: ../src/goabackend/goafacebookprovider.c:259
#: ../src/goabackend/goaflickrprovider.c:202 ../src/goabackend/goaflickrprovider.c:214
#: ../src/goabackend/goaflickrprovider.c:224 ../src/goabackend/goaflickrprovider.c:234
#: ../src/goabackend/goaflickrprovider.c:244
#: ../src/goabackend/goafoursquareprovider.c:208
#: ../src/goabackend/goafoursquareprovider.c:220
#: ../src/goabackend/goafoursquareprovider.c:231
#: ../src/goabackend/goafoursquareprovider.c:242
#: ../src/goabackend/goafoursquareprovider.c:253
#: ../src/goabackend/goafoursquareprovider.c:264
#: ../src/goabackend/goagoogleprovider.c:240 ../src/goabackend/goagoogleprovider.c:252
#: ../src/goabackend/goalastfmprovider.c:211 ../src/goabackend/goalastfmprovider.c:220
#: ../src/goabackend/goalastfmprovider.c:230 ../src/goabackend/goalastfmprovider.c:237
#: ../src/goabackend/goalastfmprovider.c:526 ../src/goabackend/goalastfmprovider.c:535
#: ../src/goabackend/goalastfmprovider.c:550 ../src/goabackend/goalastfmprovider.c:557
#: ../src/goabackend/goaoauth2provider.c:699 ../src/goabackend/goaoauth2provider.c:729
#: ../src/goabackend/goaoauth2provider.c:741
#: ../src/goabackend/goawindowsliveprovider.c:214
#: ../src/goabackend/goawindowsliveprovider.c:226
#: ../src/goabackend/goawindowsliveprovider.c:238
#, c-format
msgid "Could not parse response"
msgstr "Kunde inte tolka svar"

#: ../src/goabackend/goaflickrprovider.c:77
msgid "Flickr"
msgstr "Flickr"

#: ../src/goabackend/goaflickrprovider.c:321
msgid "Your system time is invalid. Check your date and time settings."
msgstr "Din systemtid är ogiltig. Kontrollera dina datum- och tidsinställningar."

#: ../src/goabackend/goafoursquareprovider.c:77
msgid "Foursquare"
msgstr "Foursquare"

#: ../src/goabackend/goagoogleprovider.c:76
msgid "Google"
msgstr "Google"

#. TODO: more specific
#: ../src/goabackend/goaimapauthlogin.c:94 ../src/goabackend/goasmtpauth.c:160
#, c-format
msgid "Service not available"
msgstr "Tjänsten är inte tillgänglig"

#. TODO: more specific
#: ../src/goabackend/goaimapauthlogin.c:115 ../src/goabackend/goalastfmprovider.c:543
#: ../src/goabackend/goasmtpauth.c:113
#, c-format
msgid "Authentication failed"
msgstr "Autentisering misslyckades"

#: ../src/goabackend/goaimapauthlogin.c:140
#, c-format
msgid "Server does not support PLAIN"
msgstr "Servern stöder inte PLAIN"

#: ../src/goabackend/goaimapauthlogin.c:194 ../src/goabackend/goasmtpauth.c:818
#, c-format
msgid "Server does not support STARTTLS"
msgstr "Servern stöder inte STARTTLS"

#: ../src/goabackend/goaimapsmtpprovider.c:61
msgid "IMAP and SMTP"
msgstr "IMAP och SMTP"

#. Translators: the first parameter is a field name. The second is
#. * a GOA account identifier.
#: ../src/goabackend/goaimapsmtpprovider.c:358
#: ../src/goabackend/goaimapsmtpprovider.c:416
#, c-format
msgid "Did not find %s with identity ‘%s’ in credentials"
msgstr "Hittade inte %s med identiteten ‘%s’ bland inloggningsuppgifterna"

#. Translators: the first %s is a field name. The
#. * second %s is the IMAP
#. * username (eg., rishi), and the (%s, %d)
#. * is the error domain and code.
#.
#. Translators: the first %s is a field name. The
#. * second %s is the SMTP
#. * username (eg., rishi), and the (%s, %d)
#. * is the error domain and code.
#.
#: ../src/goabackend/goaimapsmtpprovider.c:389
#: ../src/goabackend/goaimapsmtpprovider.c:449
#, c-format
msgid "Invalid %s with username ‘%s’ (%s, %d): "
msgstr "Ogiltigt %s med användarnamn ‘%s’ (%s, %d): "

#. Translators: the following four strings are used to show a
#. * combo box similar to the one in the evolution module.
#. * Encryption: None
#. *             STARTTLS after connecting
#. *             SSL on a dedicated port
#.
#: ../src/goabackend/goaimapsmtpprovider.c:636
msgid "_Encryption"
msgstr "_Kryptering"

#: ../src/goabackend/goaimapsmtpprovider.c:639
msgid "None"
msgstr "Ingen"

#: ../src/goabackend/goaimapsmtpprovider.c:642
msgid "STARTTLS after connecting"
msgstr "STARTTLS efter anslutning"

#: ../src/goabackend/goaimapsmtpprovider.c:645
msgid "SSL on a dedicated port"
msgstr "SSL på en egen port"

#: ../src/goabackend/goaimapsmtpprovider.c:701
msgid "_Name"
msgstr "_Namn"

#: ../src/goabackend/goaimapsmtpprovider.c:718
msgid "IMAP _Server"
msgstr "_IMAP-Server"

#: ../src/goabackend/goaimapsmtpprovider.c:738
msgid "SMTP _Server"
msgstr "_SMTP-Server"

#: ../src/goabackend/goaimapsmtpprovider.c:753
#: ../src/goabackend/goaimapsmtpprovider.c:1035
#: ../src/goabackend/goaimapsmtpprovider.c:1371
msgid "_Forward"
msgstr "_Framåt"

#: ../src/goabackend/goaimapsmtpprovider.c:1019
#: ../src/goabackend/goaimapsmtpprovider.c:1354
msgid "Error connecting to IMAP server"
msgstr "Fel vid anslutning till IMAP-server"

#: ../src/goabackend/goaimapsmtpprovider.c:1112
#: ../src/goabackend/goaimapsmtpprovider.c:1428
msgid "Error connecting to SMTP server"
msgstr "Fel vid anslutning till SMTP-server"

#: ../src/goabackend/goaimapsmtpprovider.c:1527
msgid "E-mail"
msgstr "E-post"

#: ../src/goabackend/goaimapsmtpprovider.c:1531
msgid "Name"
msgstr "Namn"

#: ../src/goabackend/goaimapsmtpprovider.c:1541
#: ../src/goabackend/goaimapsmtpprovider.c:1545
msgid "IMAP"
msgstr "IMAP"

#: ../src/goabackend/goaimapsmtpprovider.c:1556
#: ../src/goabackend/goaimapsmtpprovider.c:1560
msgid "SMTP"
msgstr "SMTP"

#: ../src/goabackend/goakerberosprovider.c:99
msgid "Enterprise Login (Kerberos)"
msgstr "Företagslogin (Kerberos)"

#: ../src/goabackend/goakerberosprovider.c:309
#, c-format
msgid "Could not find saved credentials for principal ‘%s’ in keyring"
msgstr "Kunde inte hitta sparade inloggningsuppgifter för huvudman ‘%s’ i nyckelringen"

#: ../src/goabackend/goakerberosprovider.c:322
#, c-format
msgid "Did not find password for principal ‘%s’ in credentials"
msgstr "Hittade inte lösenord för huvudman ‘%s’ i inloggningsuppgifterna"

#: ../src/goabackend/goakerberosprovider.c:711
msgid "_Domain"
msgstr "_Domän"

#: ../src/goabackend/goakerberosprovider.c:712
msgid "Enterprise domain or realm name"
msgstr "Namn på företagsdomän eller rike"

#: ../src/goabackend/goakerberosprovider.c:948
#: ../src/goaidentity/goaidentityservice.c:1119
msgid "Log In to Realm"
msgstr "Logga in i rike"

#: ../src/goabackend/goakerberosprovider.c:949
msgid "Please enter your password below."
msgstr "Vänligen ange ditt lösenord nedan."

#: ../src/goabackend/goakerberosprovider.c:950
msgid "Remember this password"
msgstr "Kom ihåg detta lösenord"

#: ../src/goabackend/goakerberosprovider.c:1092
#, c-format
msgid "The domain is not valid"
msgstr "Domänen är ogiltig"

#: ../src/goabackend/goakerberosprovider.c:1247
msgid "Error connecting to enterprise identity server"
msgstr "Fel vid anslutning till företagsidentitetsserver"

#: ../src/goabackend/goakerberosprovider.c:1512
#, c-format
msgid "Identity service returned invalid key"
msgstr "Identitetstjänsten returnerade en ogiltig nyckel"

#: ../src/goabackend/goalastfmprovider.c:64
msgid "Last.fm"
msgstr "Last.fm"

#: ../src/goabackend/goalastfmprovider.c:707 ../src/goabackend/goalastfmprovider.c:855
msgid "Error connecting to Last.fm"
msgstr "Fel vid anslutning till Last.fm"

#: ../src/goabackend/goamediaserverprovider.c:76
msgid "Media Server"
msgstr "Mediaserver"

#: ../src/goabackend/goamediaserverprovider.c:371
msgid ""
"Personal content can be added to your applications through a media server account."
msgstr "Personligt innehåll kan läggas till i dina program genom ett mediaserverkonto."

#: ../src/goabackend/goamediaserverprovider.c:385
msgid "Available Media Servers"
msgstr "Tillgängliga mediaservrar"

#: ../src/goabackend/goamediaserverprovider.c:415
msgid "No media servers found"
msgstr "Inga mediaservrar hittades"

#. Translators: the %d is a HTTP status code and the %s is a textual description of it
#: ../src/goabackend/goaoauth2provider.c:675 ../src/goabackend/goaoauthprovider.c:559
#, c-format
msgid "Expected status 200 when requesting access token, instead got status %d (%s)"
msgstr ""
"Förväntade status 200 vid begäran av åtkomstelement, fick istället status %d (%s)"

#: ../src/goabackend/goaoauth2provider.c:843
msgid "Authorization response: "
msgstr "Auktoriseringssvar: "

#: ../src/goabackend/goaoauth2provider.c:913
#, c-format
msgid "Authorization response: %s"
msgstr "Auktoriseringssvar: %s"

#: ../src/goabackend/goaoauth2provider.c:1060 ../src/goabackend/goaoauthprovider.c:894
msgid "Error getting an Access Token: "
msgstr "Fel vid hämtning av ett åtkomstelement: "

#: ../src/goabackend/goaoauth2provider.c:1075 ../src/goabackend/goaoauthprovider.c:907
msgid "Error getting identity: "
msgstr "Fel vid hämtning av identitet: "

#: ../src/goabackend/goaoauth2provider.c:1292 ../src/goabackend/goaoauthprovider.c:1215
#, c-format
msgid "Was asked to log in as %s, but logged in as %s"
msgstr "Frågades om att logga in som %s, men loggade in som %s"

#: ../src/goabackend/goaoauth2provider.c:1454
#, c-format
msgid "Credentials do not contain access_token"
msgstr "Inloggningsuppgifter innehåller inget åtkomstelement (access_token)"

#: ../src/goabackend/goaoauth2provider.c:1493 ../src/goabackend/goaoauthprovider.c:1443
#, c-format
msgid "Failed to refresh access token (%s, %d): "
msgstr "Misslyckades med att uppdatera åtkomstelement (%s, %d): "

#: ../src/goabackend/goaoauthprovider.c:582
#, c-format
msgid "Missing access_token or access_token_secret headers in response"
msgstr ""
"Saknar rubriker för åtkomstelement (access_token) eller åtkomstelementhemlighet "
"(access_token_secret) i svaret"

#: ../src/goabackend/goaoauthprovider.c:776
msgid "Error getting a Request Token: "
msgstr "Fel vid hämtning av ett begäranselement: "

#. Translators: the %d is a HTTP status code and the %s is a textual description of it
#: ../src/goabackend/goaoauthprovider.c:809
#, c-format
msgid "Expected status 200 for getting a Request Token, instead got status %d (%s)"
msgstr ""
"Förväntade status 200 för hämtning av ett begäranselement, fick istället status %d "
"(%s)"

#: ../src/goabackend/goaoauthprovider.c:826
#, c-format
msgid "Missing request_token or request_token_secret headers in response"
msgstr ""
"Saknar rubriker för begäranselement (request_token) eller begäranselementhemlighet i "
"svaret"

#: ../src/goabackend/goaoauthprovider.c:1399
#, c-format
msgid "Credentials do not contain access_token or access_token_secret"
msgstr ""
"Inloggningsuppgifter innehåller inte åtkomstelement (access_token) eller "
"åtkomstelementshemlighet (access_token_secret)"

#: ../src/goabackend/goaowncloudprovider.c:67
msgid "ownCloud"
msgstr "ownCloud"

#: ../src/goabackend/goaowncloudprovider.c:836
#: ../src/goabackend/goaowncloudprovider.c:1019
msgid "Error connecting to ownCloud server"
msgstr "Fel vid anslutning till ownCloud-server"

#: ../src/goabackend/goapocketprovider.c:69
msgid "Pocket"
msgstr "Pocket"

#. TODO: more specific
#: ../src/goabackend/goapocketprovider.c:218
#, c-format
msgid "No username or access_token"
msgstr "Inget användarnamn eller åtkomstelement"

#: ../src/goabackend/goaprovider.c:479
msgid "_Mail"
msgstr "_E-post"

#: ../src/goabackend/goaprovider.c:484
msgid "Cale_ndar"
msgstr "Kale_nder"

#: ../src/goabackend/goaprovider.c:489
msgid "_Contacts"
msgstr "_Kontakter"

#: ../src/goabackend/goaprovider.c:494
msgid "C_hat"
msgstr "Direkt_meddelanden"

#: ../src/goabackend/goaprovider.c:499
msgid "_Documents"
msgstr "_Dokument"

#: ../src/goabackend/goaprovider.c:504
msgid "M_usic"
msgstr "M_usik"

#: ../src/goabackend/goaprovider.c:509
msgid "_Photos"
msgstr "_Foton"

#: ../src/goabackend/goaprovider.c:514
msgid "_Files"
msgstr "_Filer"

#: ../src/goabackend/goaprovider.c:519
msgid "Network _Resources"
msgstr "Nätverks_resurser"

#: ../src/goabackend/goaprovider.c:524
msgid "_Read Later"
msgstr "_Läs senare"

#: ../src/goabackend/goaprovider.c:529
msgid "Prin_ters"
msgstr "_Skrivare"

#: ../src/goabackend/goaprovider.c:534
msgid "_Maps"
msgstr "Kar_tor"

#. Translators: This is a label for a series of
#. * options switches. For example: “Use for Mail”.
#: ../src/goabackend/goaprovider.c:563
msgid "Use for"
msgstr "Använd för"

#: ../src/goabackend/goaprovider.c:802
#, c-format
msgid "ensure_credentials_sync is not implemented on type %s"
msgstr "ensure_credentials_sync har inte implementerats för typen %s"

#. TODO: more specific
#: ../src/goabackend/goasmtpauth.c:175
#, c-format
msgid "TLS not available"
msgstr "TLS är inte tillgängligt"

#. TODO: more specific
#: ../src/goabackend/goasmtpauth.c:241
#, c-format
msgid "org.gnome.OnlineAccounts.Mail is not available"
msgstr "org.gnome.OnlineAccounts.Mail är inte tillgänglig"

#. TODO: more specific
#: ../src/goabackend/goasmtpauth.c:251
#, c-format
msgid "Failed to parse email address"
msgstr "Misslyckades med att tolka e-postadress"

#. TODO: more specific
#: ../src/goabackend/goasmtpauth.c:263
#, c-format
msgid "Cannot do SMTP authentication without a domain"
msgstr "Kan inte genomföra SMTP-autentisering utan en domän"

#. TODO: more specific
#: ../src/goabackend/goasmtpauth.c:301
#, c-format
msgid "Did not find smtp-password in credentials"
msgstr "Hittade inte smtp-lösenord bland inloggningsuppgifterna"

#. TODO: more specific
#: ../src/goabackend/goasmtpauth.c:312
#, c-format
msgid "Cannot do SMTP authentication without a password"
msgstr "Kan inte genomföra SMTP-autentisering utan ett lösenord"

#: ../src/goabackend/goasmtpauth.c:674
#, c-format
msgid "Unknown authentication mechanism"
msgstr "Okänd autentiseringsmekanism"

#: ../src/goabackend/goatelepathyprovider.c:180
#, c-format
msgid "Telepathy chat account not found"
msgstr "Telepathy-kontot för direktmeddelanden hittades inte"

#: ../src/goabackend/goatelepathyprovider.c:380
#, c-format
msgid "Failed to initialize a GOA client"
msgstr "Misslyckades med att initiera en GOA-klient"

#: ../src/goabackend/goatelepathyprovider.c:420
#, c-format
msgid "Failed to create a user interface for %s"
msgstr "Misslyckades med att skapa ett användargränssnitt för %s"

#: ../src/goabackend/goatelepathyprovider.c:535
msgid "Connection Settings"
msgstr "Anslutningsinställningar"

#: ../src/goabackend/goatelepathyprovider.c:644
msgid "Personal Details"
msgstr "Personliga detaljer"

#: ../src/goabackend/goatelepathyprovider.c:650
msgid "_OK"
msgstr "_OK"

#: ../src/goabackend/goatelepathyprovider.c:854
msgid "Cannot save the connection parameters"
msgstr "Kan inte spara anslutningsparametrarna"

#: ../src/goabackend/goatelepathyprovider.c:866
msgid "Cannot save your personal information on the server"
msgstr "Kan inte spara din personliga information på servern"

#. Connection Settings button
#: ../src/goabackend/goatelepathyprovider.c:891
msgid "_Connection Settings"
msgstr "_Anslutningsinställningar"

#. Edit Personal Information button
#: ../src/goabackend/goatelepathyprovider.c:895
msgid "_Personal Details"
msgstr "_Personliga detaljer"

#: ../src/goabackend/goautils.c:115
#, c-format
msgid "A %s account already exists for %s"
msgstr "Ett %s-konto finns redan för %s"

#. Translators: the %s is the name of the provider. eg., Google.
#: ../src/goabackend/goautils.c:137
#, c-format
msgid "%s account"
msgstr "%s-konto"

#. TODO: more specific
#: ../src/goabackend/goautils.c:181
msgid "Failed to delete credentials from the keyring"
msgstr "Misslyckades med att ta bort inloggningsuppgifter från nyckelringen"

#. TODO: more specific
#: ../src/goabackend/goautils.c:233
msgid "Failed to retrieve credentials from the keyring"
msgstr "Misslyckades med att hämta inloggningsuppgifter från nyckelringen"

#. TODO: more specific
#: ../src/goabackend/goautils.c:243
msgid "No credentials found in the keyring"
msgstr "Inloggningsuppgifter hittades inte i nyckelringen"

#: ../src/goabackend/goautils.c:256
msgid "Error parsing result obtained from the keyring: "
msgstr "Fel vid tolkning av resultatet som hämtades från nyckelringen: "

#. Translators: The %s is the type of the provider, e.g. 'google' or 'yahoo'
#: ../src/goabackend/goautils.c:299
#, c-format
msgid "GOA %s credentials for identity %s"
msgstr "GOA %s-inloggningsuppgifter för identiteten %s"

#. TODO: more specific
#: ../src/goabackend/goautils.c:316
msgid "Failed to store credentials in the keyring"
msgstr "Misslyckades med att lagra inloggningsuppgifter i nyckelringen"

#: ../src/goabackend/goautils.c:537
msgid "The signing certificate authority is not known."
msgstr "Signeringscertifikatets utfärdare är okänd."

#: ../src/goabackend/goautils.c:541
msgid ""
"The certificate does not match the expected identity of the site that it was "
"retrieved from."
msgstr ""
"Certifikatet stämmer inte överens med den förväntade identiteten för platsen som det "
"hämtades från."

#: ../src/goabackend/goautils.c:546
msgid "The certificate’s activation time is still in the future."
msgstr "Certifikatets aktiveringstid ligger i framtiden."

#: ../src/goabackend/goautils.c:550
msgid "The certificate has expired."
msgstr "Certifikatet har gått ut."

#: ../src/goabackend/goautils.c:554
msgid "The certificate has been revoked."
msgstr "Certifikatet har återkallats."

#: ../src/goabackend/goautils.c:558
msgid "The certificate’s algorithm is considered insecure."
msgstr "Certifikatets algoritm är att betrakta som osäker."

#: ../src/goabackend/goautils.c:562
msgid "Invalid certificate."
msgstr "Ogiltigt certifikat."

#. translators: %s here is the address of the web page
#: ../src/goabackend/goawebview.c:95
#, c-format
msgid "Loading “%s”…"
msgstr "Laddar ”%s”…"

#: ../src/goabackend/goawindowsliveprovider.c:77
msgid "Microsoft Account"
msgstr "Microsoft Account"

#: ../src/goaidentity/goaidentityservice.c:376
msgid "initial secret passed before secret key exchange"
msgstr "initial hemlighet skickad före utbytet av hemliga nycklar"

#: ../src/goaidentity/goaidentityservice.c:570
msgid "Initial secret key is invalid"
msgstr "Initial hemlig nyckel är ogiltig"

#: ../src/goaidentity/goaidentityservice.c:1124
#, c-format
msgid "The network realm %s needs some information to sign you in."
msgstr "Nätverksriket %s behöver en del information för att logga in dig."

#: ../src/goaidentity/goakerberosidentity.c:254
#: ../src/goaidentity/goakerberosidentity.c:263
#: ../src/goaidentity/goakerberosidentity.c:642
msgid "Could not find identity in credential cache: %k"
msgstr "Kunde inte hitta identitet i inloggningsuppgiftscache: %k"

#: ../src/goaidentity/goakerberosidentity.c:656
msgid "Could not find identity credentials in cache: %k"
msgstr "Kunde inte hitta inloggningsuppgifter i cachen: %k"

#: ../src/goaidentity/goakerberosidentity.c:700
msgid "Could not sift through identity credentials in cache: %k"
msgstr "Kunde inte gå igenom inloggningsuppgifterna i cachen: %k"

#: ../src/goaidentity/goakerberosidentity.c:718
msgid "Could not finish up sifting through identity credentials in cache: %k"
msgstr "Kunde inte slutföra genomgång av inloggningsuppgifter i cachen: %k"

#: ../src/goaidentity/goakerberosidentity.c:1013
#, c-format
msgid "No associated identification found"
msgstr "Ingen associerad identifikationsuppgift kunde hittas"

#: ../src/goaidentity/goakerberosidentity.c:1096
msgid "Could not create credential cache: %k"
msgstr "Kunde inte skapa inloggningsuppgiftscache: %k"

#: ../src/goaidentity/goakerberosidentity.c:1130
msgid "Could not initialize credentials cache: %k"
msgstr "Kunde inte initiera inloggningsuppgiftscache: %k"

#: ../src/goaidentity/goakerberosidentity.c:1147
msgid "Could not store new credentials in credentials cache: %k"
msgstr "Misslyckades med att lagra nya inloggningsuppgifter i cachen: %k"

#: ../src/goaidentity/goakerberosidentity.c:1436
#, c-format
msgid "Could not renew identity: Not signed in"
msgstr "Kunde inte förnya identitet: Inte inloggad"

#: ../src/goaidentity/goakerberosidentity.c:1448
msgid "Could not renew identity: %k"
msgstr "Kunde inte förnya identitet: %k"

#: ../src/goaidentity/goakerberosidentity.c:1465
msgid "Could not get new credentials to renew identity %s: %k"
msgstr "Kunde inte skaffa nya inloggningsuppgifter för att förnya identitet %s: %k"

#: ../src/goaidentity/goakerberosidentity.c:1507
msgid "Could not erase identity: %k"
msgstr "Kunde inte radera identitet: %k"

#: ../src/goaidentity/goakerberosidentitymanager.c:749
msgid "Could not find identity"
msgstr "Kunde inte hitta identitet"

#: ../src/goaidentity/goakerberosidentitymanager.c:840
msgid "Could not create credential cache for identity"
msgstr "Kunde inte skapa inloggningsuppgiftscache för identitet"
