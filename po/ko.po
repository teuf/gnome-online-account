# Korean translation for gnome-online-accounts.
# Copyright (C) 2011-2013 Seong-ho Cho et al.
# This file is distributed under the same license as the gnome-online-accounts package.
# Seong-ho Cho <darkcircle.0426@gmail.com>, 2011, 2012, 2013, 2014, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-online-accounts master\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-"
"online-accounts&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2015-09-17 19:37+0000\n"
"PO-Revision-Date: 2015-09-18 11:19+0900\n"
"Last-Translator: Seong-ho Cho <shcho@gnome.org>\n"
"Language-Team: Korean <gnome-kr@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 1.5.5\n"

#. TODO: more specific
#: ../src/daemon/goadaemon.c:830 ../src/daemon/goadaemon.c:1090
#, c-format
msgid "Failed to find a provider for: %s"
msgstr "제공자 정보를 찾을 수 없습니다: %s"

# ProviderType은 고유한 속성 변수이름입니다.
#: ../src/daemon/goadaemon.c:1018
msgid "IsLocked property is set for account"
msgstr "계정에 IsLocked 속성을 설정했습니다"

# ProviderType은 고유한 속성 변수이름입니다.
#. TODO: more specific
#: ../src/daemon/goadaemon.c:1078
msgid "ProviderType property is not set for account"
msgstr "계정에 ProviderType 속성에 값이 없습니다"

#. TODO: more specific
#: ../src/goabackend/goaewsclient.c:237 ../src/goabackend/goahttpclient.c:174
#, c-format
msgid "Code: %u — Unexpected response from server"
msgstr "코드: %u — 서버에서 예상치 못한 응답"

#. TODO: more specific
#: ../src/goabackend/goaewsclient.c:253
#, c-format
msgid "Failed to parse autodiscover response XML"
msgstr "자동 발견 응답 XML 해석에 실패했습니다"

#. TODO: more specific
#. Translators: the parameter is an XML element name.
#: ../src/goabackend/goaewsclient.c:264 ../src/goabackend/goaewsclient.c:279
#: ../src/goabackend/goaewsclient.c:294
#, c-format
msgid "Failed to find ‘%s’ element"
msgstr "‘%s’ 요소 찾기에 실패했습니다"

#. TODO: more specific
#: ../src/goabackend/goaewsclient.c:315
#, c-format
msgid "Failed to find ASUrl and OABUrl in autodiscover response"
msgstr "자동 발견 응답에서 ASUrl과 OABUrl을 찾는데 실패했습니다"

#  * NOTE: 그냥 음역할 것.
#: ../src/goabackend/goaexchangeprovider.c:73
msgid "Microsoft Exchange"
msgstr "마이크로소프트 익스체인지"

#. TODO: more specific
#: ../src/goabackend/goaexchangeprovider.c:311
#: ../src/goabackend/goaexchangeprovider.c:985
#: ../src/goabackend/goalastfmprovider.c:286
#: ../src/goabackend/goaowncloudprovider.c:383
#: ../src/goabackend/goaowncloudprovider.c:1128
#, c-format
msgid "Did not find password with identity ‘%s’ in credentials"
msgstr "자격 정보에 ‘%s’ 신원 정보의 암호가 없습니다"

#. Translators: the first %s is the username
#. * (eg., debarshi.ray@gmail.com or rishi), and the
#. * (%s, %d) is the error domain and code.
#.
#: ../src/goabackend/goaexchangeprovider.c:339
#: ../src/goabackend/goalastfmprovider.c:302
#: ../src/goabackend/goaowncloudprovider.c:406
#, c-format
msgid "Invalid password with username ‘%s’ (%s, %d): "
msgstr "‘%s’ 사용자 이름의 암호가 잘못되었습니다(%s, %d):"

#: ../src/goabackend/goaexchangeprovider.c:492
#: ../src/goabackend/goaimapsmtpprovider.c:700
msgid "_E-mail"
msgstr "전자메일(_E)"

#: ../src/goabackend/goaexchangeprovider.c:493
#: ../src/goabackend/goaimapsmtpprovider.c:720
#: ../src/goabackend/goaimapsmtpprovider.c:740
#: ../src/goabackend/goalastfmprovider.c:443
#: ../src/goabackend/goaowncloudprovider.c:634
msgid "_Password"
msgstr "암호(_P)"

#: ../src/goabackend/goaexchangeprovider.c:496
msgid "_Custom"
msgstr "사용자 정의(_C)"

#: ../src/goabackend/goaexchangeprovider.c:507
#: ../src/goabackend/goaimapsmtpprovider.c:719
#: ../src/goabackend/goaimapsmtpprovider.c:739
#: ../src/goabackend/goakerberosprovider.c:719
#: ../src/goabackend/goalastfmprovider.c:442
#: ../src/goabackend/goaowncloudprovider.c:633
msgid "User_name"
msgstr "사용자 이름(_N)"

#: ../src/goabackend/goaexchangeprovider.c:508
#: ../src/goabackend/goaowncloudprovider.c:632
msgid "_Server"
msgstr "서버(_S)"

#. --
#: ../src/goabackend/goaexchangeprovider.c:518
#: ../src/goabackend/goaimapsmtpprovider.c:752
#: ../src/goabackend/goakerberosprovider.c:724
#: ../src/goabackend/goalastfmprovider.c:451
#: ../src/goabackend/goaowncloudprovider.c:643
#: ../src/goabackend/goatelepathyprovider.c:649
msgid "_Cancel"
msgstr "취소(_C)"

#: ../src/goabackend/goaexchangeprovider.c:519
#: ../src/goabackend/goakerberosprovider.c:725
#: ../src/goabackend/goalastfmprovider.c:452
#: ../src/goabackend/goaowncloudprovider.c:644
msgid "C_onnect"
msgstr "연결(_O)"

#: ../src/goabackend/goaexchangeprovider.c:535
#: ../src/goabackend/goaimapsmtpprovider.c:769
#: ../src/goabackend/goakerberosprovider.c:741
#: ../src/goabackend/goalastfmprovider.c:468
#: ../src/goabackend/goaowncloudprovider.c:660
msgid "Connecting…"
msgstr "연결중…"

#: ../src/goabackend/goaexchangeprovider.c:640
#: ../src/goabackend/goaexchangeprovider.c:837
#: ../src/goabackend/goaimapsmtpprovider.c:931
#: ../src/goabackend/goaimapsmtpprovider.c:966
#: ../src/goabackend/goaimapsmtpprovider.c:1054
#: ../src/goabackend/goaimapsmtpprovider.c:1318
#: ../src/goabackend/goaimapsmtpprovider.c:1391
#: ../src/goabackend/goakerberosprovider.c:1142
#: ../src/goabackend/goalastfmprovider.c:659
#: ../src/goabackend/goalastfmprovider.c:825
#: ../src/goabackend/goamediaserverprovider.c:475
#: ../src/goabackend/goaoauth2provider.c:1034
#: ../src/goabackend/goaoauthprovider.c:863
#: ../src/goabackend/goaowncloudprovider.c:771
#: ../src/goabackend/goaowncloudprovider.c:986
#: ../src/goabackend/goatelepathyprovider.c:459
#: ../src/goabackend/goatelepathyprovider.c:508
#: ../src/goabackend/goatelepathyprovider.c:683
#, c-format
msgid "Dialog was dismissed"
msgstr "대화 상자가 닫혔습니다"

#: ../src/goabackend/goaexchangeprovider.c:678
#: ../src/goabackend/goaexchangeprovider.c:865
#: ../src/goabackend/goaimapsmtpprovider.c:996
#: ../src/goabackend/goaimapsmtpprovider.c:1089
#: ../src/goabackend/goaimapsmtpprovider.c:1342
#: ../src/goabackend/goaimapsmtpprovider.c:1416
#: ../src/goabackend/goalastfmprovider.c:693
#: ../src/goabackend/goalastfmprovider.c:844
#: ../src/goabackend/goaowncloudprovider.c:813
#: ../src/goabackend/goaowncloudprovider.c:1007
#, c-format
msgid "Dialog was dismissed (%s, %d): "
msgstr "대화 상자가 닫혔습니다(%s, %d):"

#: ../src/goabackend/goaexchangeprovider.c:691
#: ../src/goabackend/goaimapsmtpprovider.c:1009
#: ../src/goabackend/goaimapsmtpprovider.c:1102
#: ../src/goabackend/goaowncloudprovider.c:826
msgid "_Ignore"
msgstr "무시(_I)"

#: ../src/goabackend/goaexchangeprovider.c:696
#: ../src/goabackend/goaexchangeprovider.c:884
#: ../src/goabackend/goaimapsmtpprovider.c:1014
#: ../src/goabackend/goaimapsmtpprovider.c:1107
#: ../src/goabackend/goaimapsmtpprovider.c:1361
#: ../src/goabackend/goaimapsmtpprovider.c:1435
#: ../src/goabackend/goakerberosprovider.c:1252
#: ../src/goabackend/goalastfmprovider.c:705
#: ../src/goabackend/goalastfmprovider.c:861
#: ../src/goabackend/goaowncloudprovider.c:831
#: ../src/goabackend/goaowncloudprovider.c:1026
msgid "_Try Again"
msgstr "다시 시도(_T)"

#: ../src/goabackend/goaexchangeprovider.c:701
#: ../src/goabackend/goaexchangeprovider.c:877
msgid "Error connecting to Microsoft Exchange server"
msgstr "마이크로소프트 익스체인지 서버에 연결하는 도중 오류가 발생했습니다"

#: ../src/goabackend/goafacebookprovider.c:76
msgid "Facebook"
msgstr "페이스북"

#: ../src/goabackend/goafacebookprovider.c:215
#: ../src/goabackend/goaflickrprovider.c:183
#: ../src/goabackend/goafoursquareprovider.c:189
#: ../src/goabackend/goagoogleprovider.c:221
#: ../src/goabackend/goawindowsliveprovider.c:195
#, c-format
msgid ""
"Expected status 200 when requesting your identity, instead got status %d (%s)"
msgstr ""
"신원 인증을 요청할 때 상태 200을 기대했지만, %d 상태를 가져왔습니다(%s)"

#: ../src/goabackend/goafacebookprovider.c:234
#: ../src/goabackend/goafacebookprovider.c:246
#: ../src/goabackend/goafacebookprovider.c:259
#: ../src/goabackend/goaflickrprovider.c:202
#: ../src/goabackend/goaflickrprovider.c:214
#: ../src/goabackend/goaflickrprovider.c:224
#: ../src/goabackend/goaflickrprovider.c:234
#: ../src/goabackend/goaflickrprovider.c:244
#: ../src/goabackend/goafoursquareprovider.c:208
#: ../src/goabackend/goafoursquareprovider.c:220
#: ../src/goabackend/goafoursquareprovider.c:231
#: ../src/goabackend/goafoursquareprovider.c:242
#: ../src/goabackend/goafoursquareprovider.c:253
#: ../src/goabackend/goafoursquareprovider.c:264
#: ../src/goabackend/goagoogleprovider.c:240
#: ../src/goabackend/goagoogleprovider.c:252
#: ../src/goabackend/goalastfmprovider.c:211
#: ../src/goabackend/goalastfmprovider.c:220
#: ../src/goabackend/goalastfmprovider.c:230
#: ../src/goabackend/goalastfmprovider.c:237
#: ../src/goabackend/goalastfmprovider.c:526
#: ../src/goabackend/goalastfmprovider.c:535
#: ../src/goabackend/goalastfmprovider.c:550
#: ../src/goabackend/goalastfmprovider.c:557
#: ../src/goabackend/goaoauth2provider.c:699
#: ../src/goabackend/goaoauth2provider.c:729
#: ../src/goabackend/goaoauth2provider.c:741
#: ../src/goabackend/goawindowsliveprovider.c:214
#: ../src/goabackend/goawindowsliveprovider.c:226
#: ../src/goabackend/goawindowsliveprovider.c:238
#, c-format
msgid "Could not parse response"
msgstr "응답을 해석할 수 없습니다"

#: ../src/goabackend/goaflickrprovider.c:77
msgid "Flickr"
msgstr "플리커"

#: ../src/goabackend/goaflickrprovider.c:321
msgid "Your system time is invalid. Check your date and time settings."
msgstr "시스템 시간이 잘못되었습니다. 날짜와 시간 설정을 확인해보십시오."

#: ../src/goabackend/goafoursquareprovider.c:77
msgid "Foursquare"
msgstr "포스퀘어"

#: ../src/goabackend/goagoogleprovider.c:76
msgid "Google"
msgstr "구글"

#. TODO: more specific
#: ../src/goabackend/goaimapauthlogin.c:94 ../src/goabackend/goasmtpauth.c:160
#, c-format
msgid "Service not available"
msgstr "서비스를 사용할 수 없습니다"

#. TODO: more specific
#: ../src/goabackend/goaimapauthlogin.c:115
#: ../src/goabackend/goalastfmprovider.c:543
#: ../src/goabackend/goasmtpauth.c:113
#, c-format
msgid "Authentication failed"
msgstr "인증에 실패했습니다"

#: ../src/goabackend/goaimapauthlogin.c:140
#, c-format
msgid "Server does not support PLAIN"
msgstr "서버에서 PLAIN을 지원하지 않습니다"

#: ../src/goabackend/goaimapauthlogin.c:194
#: ../src/goabackend/goasmtpauth.c:818
#, c-format
msgid "Server does not support STARTTLS"
msgstr "서버에서 STARTTLS를 지원하지 않습니다"

#: ../src/goabackend/goaimapsmtpprovider.c:61
msgid "IMAP and SMTP"
msgstr "IMAP과 SMTP"

#. Translators: the first parameter is a field name. The second is
#. * a GOA account identifier.
#: ../src/goabackend/goaimapsmtpprovider.c:358
#: ../src/goabackend/goaimapsmtpprovider.c:416
#, c-format
msgid "Did not find %s with identity ‘%s’ in credentials"
msgstr "인증에서 ‘%2$s’ 신원 정보의 %1$s을(를) 찾지 못했습니다"

#. Translators: the first %s is a field name. The
#. * second %s is the IMAP
#. * username (eg., rishi), and the (%s, %d)
#. * is the error domain and code.
#.
#. Translators: the first %s is a field name. The
#. * second %s is the SMTP
#. * username (eg., rishi), and the (%s, %d)
#. * is the error domain and code.
#.
#: ../src/goabackend/goaimapsmtpprovider.c:389
#: ../src/goabackend/goaimapsmtpprovider.c:449
#, c-format
msgid "Invalid %s with username ‘%s’ (%s, %d): "
msgstr "‘%2$s’ 사용자 이름의 잘못된 %1$s입니다(%3$s, %4$d):"

#. Translators: the following four strings are used to show a
#. * combo box similar to the one in the evolution module.
#. * Encryption: None
#. *             STARTTLS after connecting
#. *             SSL on a dedicated port
#.
#: ../src/goabackend/goaimapsmtpprovider.c:636
msgid "_Encryption"
msgstr "암호화(_E)"

#: ../src/goabackend/goaimapsmtpprovider.c:639
msgid "None"
msgstr "없음"

#: ../src/goabackend/goaimapsmtpprovider.c:642
msgid "STARTTLS after connecting"
msgstr "연결 후 STARTTLS 처리"

#: ../src/goabackend/goaimapsmtpprovider.c:645
msgid "SSL on a dedicated port"
msgstr "제공 포트를 통해 SSL 연결"

#: ../src/goabackend/goaimapsmtpprovider.c:701
msgid "_Name"
msgstr "이름(_N)"

#: ../src/goabackend/goaimapsmtpprovider.c:718
msgid "IMAP _Server"
msgstr "IMAP 서버(_S)"

#: ../src/goabackend/goaimapsmtpprovider.c:738
msgid "SMTP _Server"
msgstr "SMTP 서버(_S)"

#: ../src/goabackend/goaimapsmtpprovider.c:753
#: ../src/goabackend/goaimapsmtpprovider.c:1035
#: ../src/goabackend/goaimapsmtpprovider.c:1371
msgid "_Forward"
msgstr "전달(_F)"

#: ../src/goabackend/goaimapsmtpprovider.c:1019
#: ../src/goabackend/goaimapsmtpprovider.c:1354
msgid "Error connecting to IMAP server"
msgstr "IMAP 서버에 연결 중 오류가 발생했습니다"

#: ../src/goabackend/goaimapsmtpprovider.c:1112
#: ../src/goabackend/goaimapsmtpprovider.c:1428
msgid "Error connecting to SMTP server"
msgstr "SMTP 서버에 연결 중 오류가 발생했습니다"

#: ../src/goabackend/goaimapsmtpprovider.c:1527
msgid "E-mail"
msgstr "전자메일"

#: ../src/goabackend/goaimapsmtpprovider.c:1531
msgid "Name"
msgstr "이름"

#: ../src/goabackend/goaimapsmtpprovider.c:1541
#: ../src/goabackend/goaimapsmtpprovider.c:1545
msgid "IMAP"
msgstr "IMAP"

#: ../src/goabackend/goaimapsmtpprovider.c:1556
#: ../src/goabackend/goaimapsmtpprovider.c:1560
msgid "SMTP"
msgstr "SMTP"

#: ../src/goabackend/goakerberosprovider.c:99
msgid "Enterprise Login (Kerberos)"
msgstr "기업 로그인(커베로스)"

#: ../src/goabackend/goakerberosprovider.c:309
#, c-format
msgid "Could not find saved credentials for principal ‘%s’ in keyring"
msgstr "키 모음에 저장한 ‘%s’ 본인의 자격 정보를 찾을 수 없습니다"

#: ../src/goabackend/goakerberosprovider.c:322
#, c-format
msgid "Did not find password for principal ‘%s’ in credentials"
msgstr "자격 정보에 '%s' 본인의 암호를 찾을 수 없습니다"

#: ../src/goabackend/goakerberosprovider.c:711
msgid "_Domain"
msgstr "도메인(_D)"

#: ../src/goabackend/goakerberosprovider.c:712
msgid "Enterprise domain or realm name"
msgstr "기업 도메인 또는 영역 이름"

#: ../src/goabackend/goakerberosprovider.c:948
#: ../src/goaidentity/goaidentityservice.c:1119
msgid "Log In to Realm"
msgstr "영역 로그인"

#: ../src/goabackend/goakerberosprovider.c:949
msgid "Please enter your password below."
msgstr "아래에 암호를 입력하십시오."

#: ../src/goabackend/goakerberosprovider.c:950
msgid "Remember this password"
msgstr "이 암호 저장"

#: ../src/goabackend/goakerberosprovider.c:1092
#, c-format
msgid "The domain is not valid"
msgstr "도메인이 유효하지 않습니다"

#: ../src/goabackend/goakerberosprovider.c:1247
msgid "Error connecting to enterprise identity server"
msgstr "기업 신원 서버로 연결하는데 오류가 발생했습니다"

#: ../src/goabackend/goakerberosprovider.c:1512
#, c-format
msgid "Identity service returned invalid key"
msgstr "인증 서비스에서 잘못된 키를 반환했습니다"

#: ../src/goabackend/goalastfmprovider.c:64
msgid "Last.fm"
msgstr "Last.fm"

#: ../src/goabackend/goalastfmprovider.c:707
#: ../src/goabackend/goalastfmprovider.c:855
msgid "Error connecting to Last.fm"
msgstr "Last.fm에 연결 중 오류가 발생했습니다"

#: ../src/goabackend/goamediaserverprovider.c:76
msgid "Media Server"
msgstr "미디어 서버"

#: ../src/goabackend/goamediaserverprovider.c:371
msgid ""
"Personal content can be added to your applications through a media server "
"account."
msgstr "미디어 서버 계정을 통해 개인 컨텐트를 프로그램으로 추가할 수 있습니다."

#: ../src/goabackend/goamediaserverprovider.c:385
msgid "Available Media Servers"
msgstr "존재하는 미디어 서버"

#: ../src/goabackend/goamediaserverprovider.c:415
msgid "No media servers found"
msgstr "미디어 서버가 없습니다"

#. Translators: the %d is a HTTP status code and the %s is a textual description of it
#: ../src/goabackend/goaoauth2provider.c:675
#: ../src/goabackend/goaoauthprovider.c:559
#, c-format
msgid ""
"Expected status 200 when requesting access token, instead got status %d (%s)"
msgstr ""
"접근 토큰을 요청하는데 상태 200을 기대했지만, %d 상태를 가져왔습니다(%s)"

#: ../src/goabackend/goaoauth2provider.c:843
msgid "Authorization response: "
msgstr "인증 응답: "

#: ../src/goabackend/goaoauth2provider.c:913
#, c-format
msgid "Authorization response: %s"
msgstr "인증 응답: %s"

#: ../src/goabackend/goaoauth2provider.c:1060
#: ../src/goabackend/goaoauthprovider.c:894
msgid "Error getting an Access Token: "
msgstr "접근 토큰을 가져오는데 오류가 발생했습니다:"

#: ../src/goabackend/goaoauth2provider.c:1075
#: ../src/goabackend/goaoauthprovider.c:907
msgid "Error getting identity: "
msgstr "신원 정보를 가져오는데 오류가 발생했습니다:"

#: ../src/goabackend/goaoauth2provider.c:1292
#: ../src/goabackend/goaoauthprovider.c:1215
#, c-format
msgid "Was asked to log in as %s, but logged in as %s"
msgstr "%s 계정 로그인을 요청받았지만, %s 계정으로 로그인했습니다."

#: ../src/goabackend/goaoauth2provider.c:1454
#, c-format
msgid "Credentials do not contain access_token"
msgstr "자격 정보에 access_token이 없습니다"

#: ../src/goabackend/goaoauth2provider.c:1493
#: ../src/goabackend/goaoauthprovider.c:1443
#, c-format
msgid "Failed to refresh access token (%s, %d): "
msgstr "접근 토큰을 새로 고치는데 실패했습니다(%s, %d):"

#: ../src/goabackend/goaoauthprovider.c:582
#, c-format
msgid "Missing access_token or access_token_secret headers in response"
msgstr "응답에 access_token이나 access_token_secret 헤더가 빠졌습니다"

#: ../src/goabackend/goaoauthprovider.c:776
msgid "Error getting a Request Token: "
msgstr "요청 토큰을 가져오는데 오류가 발생했습니다:"

#. Translators: the %d is a HTTP status code and the %s is a textual description of it
#: ../src/goabackend/goaoauthprovider.c:809
#, c-format
msgid ""
"Expected status 200 for getting a Request Token, instead got status %d (%s)"
msgstr ""
"요청 토큰을 가져오는데 상태 200을 기대했지만, %d 상태를 가져왔습니다(%s)"

#: ../src/goabackend/goaoauthprovider.c:826
#, c-format
msgid "Missing request_token or request_token_secret headers in response"
msgstr "응답애 request_token이나 request_token_secret 헤더가 빠졌습니다"

#: ../src/goabackend/goaoauthprovider.c:1399
#, c-format
msgid "Credentials do not contain access_token or access_token_secret"
msgstr "자격 정보에 access_token이나 access_token_secret이 없습니다"

#: ../src/goabackend/goaowncloudprovider.c:67
msgid "ownCloud"
msgstr "ownCloud"

#: ../src/goabackend/goaowncloudprovider.c:836
#: ../src/goabackend/goaowncloudprovider.c:1019
msgid "Error connecting to ownCloud server"
msgstr "ownCloud 서버에 연결하는 중 오류가 발생했습니다"

#: ../src/goabackend/goapocketprovider.c:69
msgid "Pocket"
msgstr "포켓"

#. TODO: more specific
#: ../src/goabackend/goapocketprovider.c:218
#, c-format
msgid "No username or access_token"
msgstr "사용자 이름 또는 access_token이 없습니다"

#: ../src/goabackend/goaprovider.c:479
msgid "_Mail"
msgstr "메일(_M)"

#: ../src/goabackend/goaprovider.c:484
msgid "Cale_ndar"
msgstr "달력(_N)"

#: ../src/goabackend/goaprovider.c:489
msgid "_Contacts"
msgstr "연락처(_C)"

#: ../src/goabackend/goaprovider.c:494
msgid "C_hat"
msgstr "대화(_H)"

#: ../src/goabackend/goaprovider.c:499
msgid "_Documents"
msgstr "문서(_D)"

#: ../src/goabackend/goaprovider.c:504
msgid "M_usic"
msgstr "음악(_U)"

#: ../src/goabackend/goaprovider.c:509
msgid "_Photos"
msgstr "사진(_P)"

#: ../src/goabackend/goaprovider.c:514
msgid "_Files"
msgstr "파일(_F)"

#: ../src/goabackend/goaprovider.c:519
msgid "Network _Resources"
msgstr "네트워크 자원(_R)"

#: ../src/goabackend/goaprovider.c:524
msgid "_Read Later"
msgstr "다음에 읽음(_R)"

#: ../src/goabackend/goaprovider.c:529
msgid "Prin_ters"
msgstr "프린터(_T)"

#: ../src/goabackend/goaprovider.c:534
msgid "_Maps"
msgstr "지도(_M)"

#. Translators: This is a label for a series of
#. * options switches. For example: “Use for Mail”.
#: ../src/goabackend/goaprovider.c:563
msgid "Use for"
msgstr "사용 목적"

#: ../src/goabackend/goaprovider.c:802
#, c-format
msgid "ensure_credentials_sync is not implemented on type %s"
msgstr "%s 형식에 ensure_credentials_sync를 구현하지 않았습니다"

#. TODO: more specific
#: ../src/goabackend/goasmtpauth.c:175
#, c-format
msgid "TLS not available"
msgstr "TLS를 사용할 수 없습니다"

#. TODO: more specific
#: ../src/goabackend/goasmtpauth.c:241
#, c-format
msgid "org.gnome.OnlineAccounts.Mail is not available"
msgstr "org.gnome.OnlineAccounts.Mail 설정이 없습니다"

#. TODO: more specific
#: ../src/goabackend/goasmtpauth.c:251
#, c-format
msgid "Failed to parse email address"
msgstr "전자메일 주소를 해석하는데 실패했습니다"

#. TODO: more specific
#: ../src/goabackend/goasmtpauth.c:263
#, c-format
msgid "Cannot do SMTP authentication without a domain"
msgstr "도메인 정보 없이 SMTP 인증을 처리할 수 없습니다"

#. TODO: more specific
#: ../src/goabackend/goasmtpauth.c:301
#, c-format
msgid "Did not find smtp-password in credentials"
msgstr "자격 정보에서 SMTP 암호를 찾을 수 없습니다"

#. TODO: more specific
#: ../src/goabackend/goasmtpauth.c:312
#, c-format
msgid "Cannot do SMTP authentication without a password"
msgstr "암호 없이 SMTP 인증을 처리할 수 없습니다"

#: ../src/goabackend/goasmtpauth.c:674
#, c-format
msgid "Unknown authentication mechanism"
msgstr "알 수 없는 인증 매커니즘입니다"

#: ../src/goabackend/goatelepathyprovider.c:180
#, c-format
msgid "Telepathy chat account not found"
msgstr "텔레파시 대화 계정을 찾지 못했습니다"

#: ../src/goabackend/goatelepathyprovider.c:380
#, c-format
msgid "Failed to initialize a GOA client"
msgstr "그놈 온라인 계정 클라이언트 초기화에 실패했습니다"

#: ../src/goabackend/goatelepathyprovider.c:420
#, c-format
msgid "Failed to create a user interface for %s"
msgstr "%s의 사용자 인터페이스를 만드는데 실패했습니다"

#: ../src/goabackend/goatelepathyprovider.c:535
msgid "Connection Settings"
msgstr "연결 설정"

#: ../src/goabackend/goatelepathyprovider.c:644
msgid "Personal Details"
msgstr "자세한 개인 정보"

#: ../src/goabackend/goatelepathyprovider.c:650
msgid "_OK"
msgstr "확인(_O)"

#: ../src/goabackend/goatelepathyprovider.c:854
msgid "Cannot save the connection parameters"
msgstr "연결 인자를 저장할 수 없습니다"

#: ../src/goabackend/goatelepathyprovider.c:866
msgid "Cannot save your personal information on the server"
msgstr "서버에 개인 정보를 저장할 수 없습니다"

#. Connection Settings button
#: ../src/goabackend/goatelepathyprovider.c:891
msgid "_Connection Settings"
msgstr "연결 설정(_C)"

#. Edit Personal Information button
#: ../src/goabackend/goatelepathyprovider.c:895
msgid "_Personal Details"
msgstr "자세한 개인 정보(_P)"

#: ../src/goabackend/goautils.c:115
#, c-format
msgid "A %s account already exists for %s"
msgstr "%2$s에 대한 %1$s계정이 이미 있습니다"

#. Translators: the %s is the name of the provider. eg., Google.
#: ../src/goabackend/goautils.c:137
#, c-format
msgid "%s account"
msgstr "%s 계정"

#. TODO: more specific
#: ../src/goabackend/goautils.c:181
msgid "Failed to delete credentials from the keyring"
msgstr "키 모음에서 자격 정보 삭제에 실패했습니다"

#. TODO: more specific
#: ../src/goabackend/goautils.c:233
msgid "Failed to retrieve credentials from the keyring"
msgstr "키 모음에서 자격 정보 가져오기에 실패했습니다"

#. TODO: more specific
#: ../src/goabackend/goautils.c:243
msgid "No credentials found in the keyring"
msgstr "키 모음에서 자격 정보를 찾을 수 없습니다"

#: ../src/goabackend/goautils.c:256
msgid "Error parsing result obtained from the keyring: "
msgstr "키 모음에서 가져온 결과를 해석하는데 오류가 있습니다:"

#. Translators: The %s is the type of the provider, e.g. 'google' or 'yahoo'
#: ../src/goabackend/goautils.c:299
#, c-format
msgid "GOA %s credentials for identity %s"
msgstr "%s 그놈 온라인 계정이 %s 신원 정보에 대한 자격을 부여합니다"

#. TODO: more specific
#: ../src/goabackend/goautils.c:316
msgid "Failed to store credentials in the keyring"
msgstr "키 모음에 자격 정보를 저장하는데 실패했습니다"

#: ../src/goabackend/goautils.c:537
msgid "The signing certificate authority is not known."
msgstr "알 수 없는 서명 인증 기관 입니다."

#: ../src/goabackend/goautils.c:541
msgid ""
"The certificate does not match the expected identity of the site that it was "
"retrieved from."
msgstr "인증서가 가져온 사이트에서 기대한 신원 정보와 일치하지 않습니다."

#: ../src/goabackend/goautils.c:546
msgid "The certificate’s activation time is still in the future."
msgstr "아직 인증서 활성 시기가 아닙니다."

#: ../src/goabackend/goautils.c:550
msgid "The certificate has expired."
msgstr "인증서의 기한이 끝났습니다."

#: ../src/goabackend/goautils.c:554
msgid "The certificate has been revoked."
msgstr "파기된 인증서입니다."

#: ../src/goabackend/goautils.c:558
msgid "The certificate’s algorithm is considered insecure."
msgstr "인증서 알고리즘이 안전하지 않습니다."

#: ../src/goabackend/goautils.c:562
msgid "Invalid certificate."
msgstr "인증서가 잘못되었습니다."

#. translators: %s here is the address of the web page
#: ../src/goabackend/goawebview.c:95
#, c-format
msgid "Loading “%s”…"
msgstr "“%s” 불러오는 중…"

#  * NOTE: 그냥 음역할 것.
#: ../src/goabackend/goawindowsliveprovider.c:77
msgid "Microsoft Account"
msgstr "마이크로소프트 계정"

#: ../src/goaidentity/goaidentityservice.c:376
msgid "initial secret passed before secret key exchange"
msgstr "비밀 키를 교환하기 전에 초기 비밀 키를 넘겼습니다"

#: ../src/goaidentity/goaidentityservice.c:570
msgid "Initial secret key is invalid"
msgstr "초기 비밀 키가 잘못되었습니다"

#: ../src/goaidentity/goaidentityservice.c:1124
#, c-format
msgid "The network realm %s needs some information to sign you in."
msgstr "%s 네트워크 영역에 접속하려면 약간의 정보가 더 필요합니다."

#: ../src/goaidentity/goakerberosidentity.c:254
#: ../src/goaidentity/goakerberosidentity.c:263
#: ../src/goaidentity/goakerberosidentity.c:642
msgid "Could not find identity in credential cache: %k"
msgstr "자격 정보 캐시에서 신원 정보를 찾을 수 없습니다: %k"

#: ../src/goaidentity/goakerberosidentity.c:656
msgid "Could not find identity credentials in cache: %k"
msgstr "캐시에서 신원 자격 정보를 찾을 수 없습니다: %k"

#: ../src/goaidentity/goakerberosidentity.c:700
msgid "Could not sift through identity credentials in cache: %k"
msgstr "캐시에서 신원 자격 정보로 검토할 수 없습니다: %s"

#: ../src/goaidentity/goakerberosidentity.c:718
msgid "Could not finish up sifting through identity credentials in cache: %k"
msgstr "캐시에서의 신원 자격 정보로 검토를 끝낼 수 없습니다: %k"

#: ../src/goaidentity/goakerberosidentity.c:1013
#, c-format
msgid "No associated identification found"
msgstr "관련된 신분 증명을 찾지 못했습니다"

#: ../src/goaidentity/goakerberosidentity.c:1096
msgid "Could not create credential cache: %k"
msgstr "자격 정보 캐시를 만들 수 없습니다: %k"

#: ../src/goaidentity/goakerberosidentity.c:1130
msgid "Could not initialize credentials cache: %k"
msgstr "자격 정보 캐시를 초기화 할 수 없습니다: %k"

#: ../src/goaidentity/goakerberosidentity.c:1147
msgid "Could not store new credentials in credentials cache: %k"
msgstr "자격 정보 캐시에 새로운 자격 정보를 저장할 수 없습니다: %k"

#: ../src/goaidentity/goakerberosidentity.c:1436
#, c-format
msgid "Could not renew identity: Not signed in"
msgstr "신원 정보를 새로 고칠 수 없습니다: 접속하지 않았습니다"

#: ../src/goaidentity/goakerberosidentity.c:1448
msgid "Could not renew identity: %k"
msgstr "신원 정보를 새로 고칠 수 없습니다: %k"

#: ../src/goaidentity/goakerberosidentity.c:1465
msgid "Could not get new credentials to renew identity %s: %k"
msgstr ""
"%s 신원 정보를 새로 고칠 때 활용할 새 자격 정보를 가져올 수 없습니다: %k"

#: ../src/goaidentity/goakerberosidentity.c:1507
msgid "Could not erase identity: %k"
msgstr "신원 정보를 지울 수 없습니다: %k"

#: ../src/goaidentity/goakerberosidentitymanager.c:749
msgid "Could not find identity"
msgstr "신원 정보를 찾을 수 없습니다"

#: ../src/goaidentity/goakerberosidentitymanager.c:840
msgid "Could not create credential cache for identity"
msgstr "신원에 대한 자격 정보 캐시를 만들 수 없습니다"
